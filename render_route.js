module.exports = async (route, body) => {
  return `<!DOCTYPE html>
<html lang="en">
  <head>
    <title>scratch•place</title>
    <meta charset="UTF-8" />
    <link rel="icon" href="data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'><text y='.9em' font-size='90'>🕯️</text></svg>">
    
    <script src="https://cdn.jsdelivr.net/npm/marked/marked.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/codemirror@5.63.1/lib/codemirror.min.js"></script>
    <script src="https://cdn.jsdelivr.net/combine/npm/codemirror@5.63.1,npm/codemirror@5.63.1/mode/markdown/markdown.min.js"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/codemirror@5.63.1/lib/codemirror.min.css" />

    <script src="/socket.io/socket.io.js"></script>
    <script type="text/javascript">
      var socket = io.connect('', {query: 'name=${route}'});
    </script>

    <script src="public/script.js"></script>
    <link rel="stylesheet" href="public/style.css" />
    <link rel="stylesheet" href="public/syntax.css" />
  </head>
  <body>
    <div class="main_layout">
      <textarea id="editor" style="visibility: hidden;">${body}</textarea>
    </div>
  </body>
</html>`;
};
