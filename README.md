# About Ink

Ink is a simple collaborative note taking app.

# Installing and running
Install all the dependencies using
```
npm install
```

Then you can run the local dev server using
```
npm start
```
