const $ = (el) => {
  return document.getElementById(el);
};

window.onload = () => {
  window.editor = CodeMirror.fromTextArea(document.getElementById("editor"), {
    mode: "markdown",
    theme: "uxmap",
    lineNumbers: false,
    lineWrapping: true
  });
  window.editor.on('change', function (i, op) {
    socket.emit('change', op);
    socket.emit('refresh', window.editor.getValue());
    socket.emit('updateDb', window.editor.getValue());
  });

  socket.on('refresh', function (data) {
    window.editor.setValue(data.body);
  });
  socket.on('change', function (data) {
    window.editor.replaceRange(data.text, data.from, data.to);
  });
};
