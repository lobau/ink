const fs = require("fs");
const path = require("path");

const render_route = require("./render_route.js");
const error_page = require("./error_page.js");
const create_page = require("./create_page.js");

var db = {};

const generateUnique = (length = 24) => {
  let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let str = "";
  for (let i = 0; i < length; i++) {
    str += chars.charAt(Math.floor(Math.random() * chars.length));
  }
  return str;
};

var app = require("http")
  .createServer(function (req, res) {
    if (req.url === "/") {
      let route = generateUnique();
      create_page(route).then(
        function (html) {
          res.writeHead(200, { "Content-Type": "text/html" });
          res.write(html);
          res.end();
        }.bind(res));
    } else if (req.url.match("public/favicon.svg")) {
      var fileStream = fs.createReadStream(
        path.join(__dirname + "/public/favicon.svg"),
        "UTF-8"
      );
      res.writeHead(200, { "Content-Type": "image/svg+xml" });
      fileStream.pipe(res);
    } else if (req.url.match("public/style.css")) {
      var fileStream = fs.createReadStream(
        path.join(__dirname + "/public/style.css"),
        "UTF-8"
      );
      res.writeHead(200, { "Content-Type": "text/css" });
      fileStream.pipe(res);
    } else if (req.url.match("public/syntax.css")) {
      var fileStream = fs.createReadStream(
        path.join(__dirname + "/public/syntax.css"),
        "UTF-8"
      );
      res.writeHead(200, { "Content-Type": "text/css" });
      fileStream.pipe(res);
    } else if (req.url.match("public/script.js")) {
      var fileStream = fs.createReadStream(
        path.join(__dirname + "/public/script.js"),
        "UTF-8"
      );
      res.writeHead(200, { "Content-Type": "text/javascript" });
      fileStream.pipe(res);
    } else if (req.url.match(/^\/([A-Za-z0-9]{16})/)) {
      var route = req.url.slice(1);

      if(db[route] === undefined) {
        db[route] = `# Hello there\nShare this page link with anybody and start collaborating!`;
      }
      body = db[route];
      render_route(route, body).then(
        function (html) {
          res.writeHead(200, { "Content-Type": "text/html" });
          res.write(html);
          res.end();
        }.bind(res)
      );
    } else {
      error_page().then(
        function (html) {
          res.writeHead(404, { "Content-Type": "text/html" });
          res.write(html);
          res.end();
        }.bind(res));
    }
  })
  .listen(process.env.PORT || 8080);

var io = require("socket.io")(app);

io.on("connection", (socket) => {
  const route = socket.handshake.query.name;
  socket.join(route);

  console.log("A user has connected from " + route);

  socket.on("refresh", function () {
    body = db[route];
  });
  socket.on("updateDb", function (body) {
    db[route] = body;
  });
  socket.on("change", function (op) {
    if (
      op.origin == "+input" ||
      op.origin == "paste" ||
      op.origin == "+delete"
    ) {
      socket.broadcast.to(route).emit("change", op);
    }
  });
  socket.on("disconnect", function () {
    // TODO: save doc on the last disconnect?
  });
});
