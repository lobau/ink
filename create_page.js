module.exports = async (route) => {
    return `
        <!DOCTYPE html>
        <html lang="en">
            <head>
                <title>scratch•place</title>
                <meta charset="UTF-8" />
                <link rel="icon" href="data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'><text y='.9em' font-size='90'>🕯️</text></svg>">
                <link rel="stylesheet" href="public/style.css" />
                <meta http-equiv="Refresh" content="1.5; url='/${route}'" />
            </head>
            <body>
                <div id="center"><h1>🕯️</h1><p>Creating note…</p></div>
            </body>
        </html>`;
};
